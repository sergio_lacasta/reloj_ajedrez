library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity EDGEDTCTR is
    port (
        CLK : in std_logic;
        SYNC_IN : in std_logic;
        EDGE : out std_logic
    );
end EDGEDTCTR;
architecture Behavioral of EDGEDTCTR is
    signal sreg : std_logic_vector(2 downto 0); --Se crea una variable vector de tres bits llamada sreg
begin
    process (CLK)
    begin
        if rising_edge(CLK) then --Cuando hay un pulso de reloj:
             sreg <= sreg(1 downto 0) & SYNC_IN; --La señal sreg se mueve un bit a la izq. 
        end if;                                  --y en la derecha del todo se le pone el valor de SYNC_IN 
    end process;
    
    with sreg select
        EDGE <= '1' when "100",--La salida EDGE será 1 cuando sreg sea "100", cuando pase un ciclo de reloj
                '0' when others;
end Behavioral;

