library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity top is --Entradas y salidas
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           switch : in  STD_LOGIC;
           display_number : out  STD_LOGIC_VECTOR (6 downto 0);
           display_selection_out1 : out  STD_LOGIC_VECTOR (7 downto 0);
           startstop : in std_logic 
           );
          
end top;

architecture Behavioral of top is
--señales intermedias
	signal clk_counter:std_logic;
	
	signal clk_display:std_logic;
	
    signal cuenta_unid_seg:std_logic_vector(3 downto 0);
	signal cuenta_dec_seg:std_logic_vector(3 downto 0);
	signal cuenta_unid_min:std_logic_vector(3 downto 0);
	
	
	signal salida_unid_seg:std_logic;
	signal salida_dec_seg:std_logic;
	signal salida_unid_min:std_logic;
	
	
	signal segment_unid_seg:std_logic_vector(6 downto 0);
	signal segment_dec_seg:std_logic_vector(6 downto 0);
	signal segment_unid_min:std_logic_vector(6 downto 0);
	
	 signal display_number1 :STD_LOGIC_VECTOR (6 downto 0);
     signal display_selection1 :STD_LOGIC_VECTOR (3 downto 0);
	
	--segundo reloj
    signal cuenta_unid_seg2:std_logic_vector(3 downto 0);
	signal cuenta_dec_seg2:std_logic_vector(3 downto 0);
	signal cuenta_unid_min2:std_logic_vector(3 downto 0);
   
    signal salida_unid_seg2:std_logic;
	signal salida_dec_seg2:std_logic;
	signal salida_unid_min2:std_logic;
	
	
	signal segment_unid_seg2:std_logic_vector(6 downto 0);
	signal segment_dec_seg2:std_logic_vector(6 downto 0);
	signal segment_unid_min2:std_logic_vector(6 downto 0);

	 signal display_number2 :STD_LOGIC_VECTOR (6 downto 0);
     signal display_selection2 :STD_LOGIC_VECTOR (3 downto 0);
	
	signal reset : std_logic;
    signal sync_out : std_logic;
    signal edge : std_logic;

	
	COMPONENT clk_divider --Componente para ajustar tiempo 
	GENERIC (frec: integer:=50000000);
	PORT( --Entradas y salidas
		clk : IN std_logic;
		reset : IN std_logic;          
		clk_out : OUT std_logic
		);
	END COMPONENT;
	
	COMPONENT counter --Contador descendente del primer reloj
	generic (tope:unsigned (3 downto 0);
         load:unsigned (3 downto 0)
         );
	PORT( --Entradas y salidas
		clk : IN std_logic;
		reset : IN std_logic;
		enable : IN std_logic;          
		count : OUT std_logic_vector(3 downto 0);
		salida : OUT std_logic;
		  startstop : in std_logic
		);
	END COMPONENT;
	
	COMPONENT counter2 --Contador descendente del segundo reloj
	generic (tope:unsigned (3 downto 0); --Valor máximo al que llega un dígito
         load:unsigned (3 downto 0) --Valor de inicio de cada digito
         );
	PORT( --Entradas y salidas
		clk : IN std_logic;
		reset : IN std_logic;
		enable : IN std_logic;          
		count2 : OUT std_logic_vector(3 downto 0);
		salida2 : OUT std_logic;
		  startstop : in std_logic
		);
	END COMPONENT;

	COMPONENT decoder
	PORT( --Entradas y salidas
		code : IN std_logic_vector(3 downto 0);          
		led : OUT std_logic_vector(6 downto 0)
		);
	END COMPONENT;


   COMPONENT display_refresh
	PORT( --Entradas y salidas
		clk : IN std_logic;
		segment_unid_seg : IN std_logic_vector(6 downto 0);
		segment_unid_min : IN std_logic_vector(6 downto 0);
		segment_dec_seg : IN std_logic_vector(6 downto 0);
     	display_number1 : OUT std_logic_vector(6 downto 0);
     	segment_unid_seg2 : IN std_logic_vector(6 downto 0);
		segment_unid_min2 : IN std_logic_vector(6 downto 0);
    	segment_dec_seg2 : IN std_logic_vector(6 downto 0);
	    display_selection1 : OUT std_logic_vector(7 downto 0)
	    
		);
	END COMPONENT;
	
	
	
	COMPONENT SYNCHRNZR
PORT ( --Entradas y salidas
        CLK : in std_logic;
        ASYNC_IN : in std_logic;
        SYNC_OUT : out std_logic
    );
END COMPONENT;

COMPONENT EDGEDTCTR
PORT ( --Entradas y salidas
        CLK : in std_logic;
        SYNC_IN : in std_logic;
        EDGE : out std_logic
    );
END COMPONENT;

 
   signal aux1,aux2,aux3,aux4: std_logic; --Señales auxiliares

begin

	Inst_clk_divider_counter: clk_divider generic map (frec=>5000000) PORT MAP(
		clk => clk,
		reset => reset,
		clk_out => clk_counter
	);
	
	Inst_clk_divider_display: clk_divider generic map (frec=>100000) PORT MAP(
		clk => clk,
		reset => reset,
		clk_out => clk_display
	);
	
	--Reloj 1
   Inst_counter_unid_seg: counter generic map (tope=>"1001" , load =>"0000") PORT MAP(--
		clk => clk_counter,
		reset => reset,
		enable => switch,
		count => cuenta_unid_seg,
		startstop => startstop,
		salida => salida_unid_seg
	);
	
	aux1<=salida_unid_seg and switch and startstop; --Cuando salida_unid_seg=1, switch=1 y starstop=1,aux1=1 y el contador pasa a contar decenas de segundos.
	
	 Inst_counter_dec_seg: counter generic map (tope=>"0101" , load =>"0000") PORT MAP(
		clk => clk_counter,
		reset => reset,
		enable => aux1,
		count => cuenta_dec_seg,
		startstop => startstop,
		salida => salida_dec_seg
	);
	
	aux2<=salida_dec_seg and salida_unid_seg and switch and startstop;--Cuando salida_unid_seg=1, salida_dec_seg=1, switch=1 y startstop=1, aux el contador1 pasa a contar unidades de minuto.
	  Inst_counter_unid_min: counter generic map (tope=>"1001" , load =>"0101") PORT MAP(--Tope='9', load='5'
		clk => clk_counter,
		reset => reset,
		enable => aux2,
		count => cuenta_unid_min,
		startstop => startstop,
		salida => salida_unid_min
	);
--Reloj 2
	 Inst_counter_unid_seg2: counter2 generic map (tope=>"1001" , load =>"0000") PORT MAP(--tope='9' load='0'
		clk => clk_counter,
		reset => reset,
		enable => switch,
		count2 => cuenta_unid_seg2,
		startstop => startstop,
		salida2 => salida_unid_seg2
	);
	
	aux3<=not(salida_unid_seg2 and not switch and startstop);--Cuando salida_unid_seg2=1, switch=0 y startstop=1, aux3=0 y el contador2 pasa a contar decenas de segundo.
	
	 Inst_counter_dec_seg2: counter2 generic map (tope=>"0101" , load =>"0000") PORT MAP( --tope'5', load='0'
		clk => clk_counter,
		reset => reset,
		enable => aux3,
		count2 => cuenta_dec_seg2,
		startstop => startstop,
		salida2 => salida_dec_seg2
	);
	
	aux4<=not(salida_dec_seg2 and salida_unid_seg2 and not switch and startstop);--Cuando salida_unid_seg2=1,salida_Dec_min2=1, switch=0 y startstop=1, aux3=0 y el contador2 pasa a contar unidades de minuto.
	
	  Inst_counter_unid_min2: counter2 generic map (tope=>"1001" , load =>"0101") PORT MAP( --tope='9' y load='5'
		clk => clk_counter,
		reset => reset,
		enable => aux4,
		count2 => cuenta_unid_min2,
		startstop => startstop,
		salida2 => salida_unid_min2
	);
	


	Inst_decoder_unid_seg2: decoder PORT MAP(
		code => cuenta_unid_seg2,
		led => segment_unid_seg2
	);
	
	Inst_decoder_dec_seg2: decoder PORT MAP(
		code => cuenta_dec_seg2,
		led => segment_dec_seg2
	);
	
	Inst_decoder_unid_min2: decoder PORT MAP(
		code => cuenta_unid_min2,
		led => segment_unid_min2
	);
	

	
	Inst_display_refresh: display_refresh PORT MAP(
		clk => clk_display,
		segment_unid_seg => segment_unid_seg,
		segment_unid_min => segment_unid_min,
		segment_dec_seg => segment_dec_seg,
		display_number1 => display_number,
		segment_unid_seg2 => segment_unid_seg2,
		segment_unid_min2 => segment_unid_min2,
		segment_dec_seg2 => segment_dec_seg2,
		display_selection1 => display_selection_out1 
	
	);
	
	
	Inst_SYNCHRNZR: SYNCHRNZR PORT MAP (
        ASYNC_IN => rst,
        clk => clk,
        SYNC_OUT => sync_out
    );

    Inst_EDGEDTCTR: EDGEDTCTR PORT MAP (
        SYNC_IN => sync_out,
        clk => clk,
        EDGE => reset
    );	
    
    Inst_decoder_unid_seg: decoder PORT MAP(
		code => cuenta_unid_seg,
		led => segment_unid_seg
	);
	   Inst_decoder_dec_seg: decoder PORT MAP(
		code => cuenta_dec_seg,
		led => segment_dec_seg
	);
	
	  Inst_decoder_unid_min: decoder PORT MAP(
		code => cuenta_unid_min,
		led => segment_unid_min
	);
	
	
	
end Behavioral;
