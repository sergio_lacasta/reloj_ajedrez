library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity SYNCHRNZR is
    port (
        CLK : in std_logic;
        ASYNC_IN : in std_logic;
        SYNC_OUT : out std_logic
    );
end SYNCHRNZR;

architecture Behavioral of SYNCHRNZR is
    signal sreg : std_logic_vector(1 downto 0); --Se crea una señal vector de dos bits llamada sreg
begin
    process (CLK)
    begin
        if rising_edge(CLK) then --Cuando hay un flanco positivo de reloj:
            sync_out <= sreg(1); --Se le asigna a la salida sincrona el valor de la posición 1 de la señal creada sreg
            sreg <= sreg(0) & async_in;-- Ahora a la señal sreg se le asigna un nuevo valor para el siguiente golpe de reloj
        end if;
    end process;
end Behavioral;
