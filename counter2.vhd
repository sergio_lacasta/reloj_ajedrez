library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity counter2 is

generic (tope:unsigned (3 downto 0);
         load:unsigned (3 downto 0)
         );
 
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           enable : in  STD_LOGIC;
           count2 : out  STD_LOGIC_VECTOR (3 downto 0);
           salida2 : out  STD_LOGIC;
           startstop : in std_logic);
end counter2;

architecture Behavioral of counter2 is
signal cnt: unsigned (3 downto 0);
begin

	process (clk, reset, enable)
	
	begin
	  if (reset='1') then
	     cnt<= load; 
		 salida2<='1';--si reset='1' se activa la salida 
	  elsif clk'event and clk='1' then
			if enable ='0' and startstop = '1' then
			  if cnt>0 then    --cuando la cuenta no es cero baja el contador
					cnt<=cnt-1;
					salida2<='0';
					if cnt=1 then  --cuando está justo antes de cambiar activa las salidas 
						salida2<='1'; --Salida se activa
					end if;
			   else                     --cuando la cuenta es cero se pone al tope, tope es 9 en unidades y 5 en decimas
					cnt <= tope;
					salida2<='0';
				end if;
			end if;
	  end if;
	end process;
   count2 <= std_logic_vector(cnt);

end Behavioral;
