
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;



entity display_refresh is --Actualiza los digitos del display 
    Port ( clk : in  STD_LOGIC;
          reset: in std_logic;
           segment_unid_seg : IN std_logic_vector(6 downto 0);
		   segment_unid_min : IN std_logic_vector(6 downto 0);
    	   segment_dec_seg : IN std_logic_vector(6 downto 0);
	       display_number1 : out  STD_LOGIC_VECTOR (6 downto 0);
	        segment_unid_seg2 : IN std_logic_vector(6 downto 0);
		   segment_unid_min2 : IN std_logic_vector(6 downto 0);
    	   segment_dec_seg2 : IN std_logic_vector(6 downto 0);
	      
           display_selection1 : out  STD_LOGIC_VECTOR (7 downto 0)
           
           );
end display_refresh;

architecture Behavioral of display_refresh is

begin

	muestra_displays:process (clk, segment_unid_seg, segment_unid_min, segment_dec_seg)
	variable cnt:integer range 0 to 7;
	begin
		if (clk'event and clk='1') then 
			if cnt=7 then
				cnt:=0;
			else
				cnt:=cnt+1;
			end if;
		end if;
		
		case cnt is
		---------Reloj 1
				when 0 => if (reset='1')  then
				display_selection1<="11111110";--Si reset='1', se pselecciona ultimo digito (Activo a nivel bajo)
		                  display_number1 <= "1111111";
		          
		                  else 
				display_selection1<="11111110";
						    display_number1<=segment_unid_seg;-- Al digito seleccionado se le asigna la combinacion de segmentos
						    end if;
				
				when 1 => if (reset='1')  then
				display_selection1<="11111101";
		                  display_number1 <= "1111111";
		                   
		                  else
				display_selection1<="11111101";
						    display_number1<=segment_dec_seg;
						    end if;
				
				when 2 => if (reset='1')  then
				 display_selection1<="11111011";
		                  display_number1 <= "1111111";
		                   
		                  else
		                  display_selection1<="11111011";
						    display_number1<=segment_unid_min;
						    end if;
				
				when 3 => if (reset='1')  then
				display_selection1<="11110111";--Cuarto digito seleccionado
		                  display_number1 <= "1111111";
		                   
		                  else
		                  display_selection1<="11110111";
						    display_number1<="0000001";	--EL cuarto digito de cada reloj va a ser siempre 0 porque nunca van a haber decenas de minuto.
						    end if;			
	-------------------------	Reloj 2				    
				when 4 => if (reset='1')  then
				display_selection1<="11101111";
		                  display_number1 <= "1111111";
		                  else 
				display_selection1<="11101111";
						    display_number1<=segment_unid_seg2;
						    end if;
				
				when 5 => if (reset='1')  then
				display_selection1<="11011111";
		                  display_number1 <= "1111111";
		                  else
				display_selection1<="11011111";
						    display_number1<=segment_dec_seg2;
						    end if;
				
				when 6 => if (reset='1')  then
				 display_selection1<="10111111";
		                  display_number1 <= "1111111";
		                  
		                  else
		                  display_selection1<="10111111";
						    display_number1 <=segment_unid_min2;
						    end if;
				
				when 7 => if (reset='1')  then
				display_selection1<="01111111";
		                  display_number1 <= "1111111";
		                  else
		                  display_selection1<="01111111";
						    display_number1<="0000001";	
						    end if;				    
						    
			end case;

	end process;

end Behavioral;
