library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity display_refresh_tb is
end;

architecture bench of display_refresh_tb is

  component display_refresh
      Port ( clk : in  STD_LOGIC;
            reset: in std_logic;
             segment_unid_seg : IN std_logic_vector(6 downto 0);
  		   segment_unid_min : IN std_logic_vector(6 downto 0);
      	   segment_dec_seg : IN std_logic_vector(6 downto 0);
  	       display_number1 : out  STD_LOGIC_VECTOR (6 downto 0);
  	        segment_unid_seg2 : IN std_logic_vector(6 downto 0);
  		   segment_unid_min2 : IN std_logic_vector(6 downto 0);
      	   segment_dec_seg2 : IN std_logic_vector(6 downto 0);
             display_selection1 : out  STD_LOGIC_VECTOR (7 downto 0)
             );
  end component;

  signal clk: STD_LOGIC;
  signal reset: std_logic;
  signal segment_unid_seg: std_logic_vector(6 downto 0);
  signal segment_unid_min: std_logic_vector(6 downto 0);
  signal segment_dec_seg: std_logic_vector(6 downto 0);
  signal display_number1: STD_LOGIC_VECTOR (6 downto 0);
  signal segment_unid_seg2: std_logic_vector(6 downto 0);
  signal segment_unid_min2: std_logic_vector(6 downto 0);
  signal segment_dec_seg2: std_logic_vector(6 downto 0);
  signal display_selection1: STD_LOGIC_VECTOR (7 downto 0) ;

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  uut: display_refresh port map ( clk                => clk,
                                  reset              => reset,
                                  segment_unid_seg   => segment_unid_seg,
                                  segment_unid_min   => segment_unid_min,
                                  segment_dec_seg    => segment_dec_seg,
                                  display_number1    => display_number1,
                                  segment_unid_seg2  => segment_unid_seg2,
                                  segment_unid_min2  => segment_unid_min2,
                                  segment_dec_seg2   => segment_dec_seg2,
                                  display_selection1 => display_selection1 );

  stimulus: process
  begin
  
    

    -- Ejemplo 1
segment_unid_seg <= "0010010";
segment_unid_min <= "0100000";
segment_dec_seg <= "0100100";
segment_unid_seg2 <= "0100110";
segment_unid_min2 <= "0101011";
segment_dec_seg2 <= "0101111";
wait for 50ns;
    
    -- Ejemplo 2

segment_unid_seg <= "0000001";
segment_unid_min <= "0001111";
segment_dec_seg <= "0001111";
segment_unid_seg2 <= "0111011";
segment_unid_min2 <= "1110011";
segment_dec_seg2 <= "0001010";
wait for 50ns;

     -- Ejemplo 3 (caso imposible >9)

segment_unid_seg <= "1111110";
segment_unid_min <= "1111110";
segment_dec_seg <= "1111110";
segment_unid_seg2 <= "1111110";
segment_unid_min2 <= "1111110";
segment_dec_seg2 <= "1111110";
wait for 50ns;

     -- Ejemplo 4

segment_unid_seg <= "0000000";
segment_unid_min <= "0000000";
segment_dec_seg <= "0000000";
segment_unid_seg2 <= "0000000";
segment_unid_min2 <= "0000000";
segment_dec_seg2 <= "0000000";

  wait for 50ns;

  reset <= '1';
 
  wait for 100ns;
 
  reset <= '1';
  
  wait for 100ns;
 
    stop_the_clock <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;