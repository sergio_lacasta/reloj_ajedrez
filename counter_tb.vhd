library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity counter_tb is
end;

architecture bench of counter_tb is

  component counter
  generic (tope:unsigned (3 downto 0);
           load:unsigned (3 downto 0)
           );
      Port ( clk : in  STD_LOGIC;
             reset : in  STD_LOGIC;
             enable : in  STD_LOGIC;
             count : out  STD_LOGIC_VECTOR (3 downto 0);
             salida : out  STD_LOGIC;
       startstop: in std_logic
             );
  end component;

  signal clk: STD_LOGIC;
  signal reset: STD_LOGIC;
  signal enable: STD_LOGIC;
  signal count: STD_LOGIC_VECTOR (3 downto 0);
  signal salida: STD_LOGIC;
  signal startstop: std_logic ;

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin


  uut: counter generic map ( tope      => "1001",
                             load      => "0000" )
                  port map ( clk       => clk,
                             reset     => reset,
                             enable    => enable,
                             count     => count,
                             salida    => salida,
                             startstop => startstop );

  stimulus: process
  begin
  

     
    
        reset <= '1';
        enable <= '1';
        startstop <= '1';
        wait for 10ns;
        
        reset <= '1';
        enable <= '0';
        wait for 10ns;
        
        reset <= '0';
        enable <= '1';
        startstop <= '1';
        wait for 140ns;
        
        reset <= '1';
    
        wait for 10ns;
        
  
        reset <= '0';
        enable <= '1';
        startstop <= '1';
        wait for 140ns;

        reset <= '0';
        enable <= '1';
        startstop <= '0';
        wait for 140ns;
        
        reset <= '0';
        enable <= '1';
        startstop <= '1';
        wait for 140ns;
        
    stop_the_clock <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;

