library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity counter is

generic (tope:unsigned (3 downto 0);
         load:unsigned (3 downto 0)
         );
 
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           enable : in  STD_LOGIC;
           count : out  STD_LOGIC_VECTOR (3 downto 0);
           salida : out  STD_LOGIC;
     startstop: in std_logic
           );
end counter;

architecture Behavioral of counter is
signal cnt: unsigned (3 downto 0);

begin

	process (clk, reset, enable)
	
	begin
	  if (reset='1') then
	     cnt<= load; 
		 salida<='1';
		
	  elsif clk'event and clk='1' then
			if enable ='1' and startstop ='1' then
			  if cnt>0 then    --cuando la cuenta no es cero baja
					cnt<=cnt-1;
					
					salida<='0';
					if cnt=1 then  --cuando está justo antes de cambiar activa las salidas 
						salida<='1';
					end if;
			   else                     --cuando la cuenta es cero se pone al tope, tope es 9 en unidades y 5 en decimas
					cnt <= tope;
					salida<='0';
					
				end if;
			end if;
			
			
	  end if;

	end process;
   count <= std_logic_vector(cnt);
   
end Behavioral;
